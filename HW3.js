//////////////  1

let userNumber;

do {
    userNumber = prompt('Please, enter any number', [userNumber])
} while (userNumber % 1 != 0)

if (userNumber < 5) {
    console.log("Sorry, no numbers");
} else {
    for (let i = 0; i <= userNumber; i++) {
        if (i % 5 === 0) {
            console.log(i);
        }
    }
}


//////////////  2

let m,n;

while (m % 1 != 0) {
    m = +prompt('Please, enter first number')
}
while (n % 1 != 0) {
    n = +prompt('Please, enter second number')
}

if (m == n) {
    console.log(m);
} else if (m < n) {
    for (let i = m; i <=n; i++)
    console.log(i);
} else {
    for (let i = n; i <=m; i++)
    console.log(i);
}